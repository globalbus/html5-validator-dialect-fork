package net.sourceforge.html5val;

import java.lang.annotation.Annotation;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import net.sourceforge.html5val.performers.DigitsPerformer;
import net.sourceforge.html5val.performers.EmailPerformer;
import net.sourceforge.html5val.performers.LengthPerformer;
import net.sourceforge.html5val.performers.MaxPerformer;
import net.sourceforge.html5val.performers.MinPerformer;
import net.sourceforge.html5val.performers.NotBlankPerformer;
import net.sourceforge.html5val.performers.NotEmptyPerformer;
import net.sourceforge.html5val.performers.NotNullPerformer;
import net.sourceforge.html5val.performers.PatternPerformer;
import net.sourceforge.html5val.performers.RangePerformer;
import net.sourceforge.html5val.performers.SizePerformer;
import net.sourceforge.html5val.performers.URLPerformer;

import org.thymeleaf.dom.Element;

public class ValidationPerformerFactory {

	private static final ValidationPerformerFactory SINGLE_INSTANCE = new ValidationPerformerFactory();

	private static final ValidationPerformer<?> NULL_PERFORMER = new ValidationPerformer<Annotation>() {
		public Class<Annotation> getConstraintClass() {
			return null;
		}

		public void putValidationCodeInto(Annotation constraint, Element element) {
			//
		}
	};

	private final Map<Class<?>, ValidationPerformer<?>> performers;

	private ValidationPerformerFactory() {
		this.performers = Collections.synchronizedMap(new HashMap<Class<?>, ValidationPerformer<?>>());
		addPerformer(new DigitsPerformer());
		addPerformer(new EmailPerformer());
		addPerformer(new MaxPerformer());
		addPerformer(new MinPerformer());
		addPerformer(new RangePerformer());
		addPerformer(new NotEmptyPerformer());
		addPerformer(new NotNullPerformer());
		addPerformer(new NotBlankPerformer());
		addPerformer(new PatternPerformer());
		addPerformer(new SizePerformer());
		addPerformer(new LengthPerformer());
		addPerformer(new URLPerformer());
	}

	private void addPerformer(ValidationPerformer<?> performer) {
		this.performers.put(performer.getConstraintClass(), performer);
	}

	/**
	 * Add a custom ValidationPerformer to the list of performers.
	 */
	public static void addCustomPerformer(ValidationPerformer<?> performer) {
		SINGLE_INSTANCE.addPerformer(performer);
	}

	public static ValidationPerformer<? extends Annotation> getPerformerFor(Annotation constraint) {
		return getPerformerFor(constraint.annotationType());
	}

	public static ValidationPerformer<? extends Annotation> getPerformerFor(Class<? extends Annotation> constraint) {
		return SINGLE_INSTANCE.getPerformerForConstraint(constraint);
	}

	private ValidationPerformer<? extends Annotation> getPerformerForConstraint(Class<? extends Annotation> constraint) {
		ValidationPerformer<?> performer = this.performers.get(constraint);
		if (performer != null)
			return performer;
		return NULL_PERFORMER;
	}

    private boolean isPerformerForConstraint(ValidationPerformer performer, Annotation constraint) {
        Class constraintClass = constraint.getClass();
        return performer.getConstraintClass().isAssignableFrom(constraintClass);
    }
	boolean customValidityEnabled = true;

	public static boolean isCustomValidityEnabled() {
		return SINGLE_INSTANCE.customValidityEnabled;
	}
	public static void setCustomValidityEnabled(boolean enabled) {
		SINGLE_INSTANCE.customValidityEnabled=enabled;
	}
}
