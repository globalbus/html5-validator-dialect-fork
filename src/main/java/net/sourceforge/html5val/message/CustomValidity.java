package net.sourceforge.html5val.message;

import java.lang.annotation.Annotation;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

import javax.validation.MessageInterpolator.Context;
import javax.validation.ValidatorFactory;
import javax.validation.metadata.BeanDescriptor;
import javax.validation.metadata.ConstraintDescriptor;
import javax.validation.metadata.PropertyDescriptor;

import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.servlet.support.RequestContext;
import org.thymeleaf.Arguments;
import org.thymeleaf.dom.Element;
import org.thymeleaf.spring4.naming.SpringContextVariableNames;

public class CustomValidity {
	private static final String ANNOTATION_MESSAGE = "message";
	private static final String ONCHANGE_ATTR = "onchange";
	private static final String ONINVALID_ATTR = "oninvalid";
	private static final Pattern ROOT_ELEMENT = Pattern.compile("\\$\\{(.*?)\\}");
	private static final Pattern INTERNAL = Pattern.compile("\\{(.*?)\\}");

	private Object realObject;
	private String root;
	private Element element;
	private String attributeName;
	private Arguments arguments;
	private ValidatorFactory validatorFactory;
	private RequestContext requestContext;
	private BeanDescriptor beanDescriptor;
	private MyValidationAdapter validator;
	private boolean disabled;

	public CustomValidity(Arguments arguments, Element element, String attributeName, Object realObject) {
		this.arguments = arguments;
		this.element = element;
		this.attributeName = attributeName;
		this.realObject = realObject;
		try {
			this.root = ROOT_ELEMENT.matcher(this.element.getAttributeValue(this.attributeName)).replaceFirst("$1");
			this.requestContext = (RequestContext) this.arguments.getContext().getVariables()
					.get(SpringContextVariableNames.SPRING_REQUEST_CONTEXT);
			this.validatorFactory = this.requestContext.getWebApplicationContext().getBean(ValidatorFactory.class);
			this.validator = new MyValidationAdapter(this.validatorFactory.getValidator());
			this.beanDescriptor = this.validatorFactory.getValidator().getConstraintsForClass(
					this.realObject.getClass());
		} catch (Exception e) {
			this.disabled = true;
		}
	}

	public void processField(Element fieldElement, String fieldName) {
		if(this.disabled)
			return;
		BeanPropertyBindingResult binding = new BeanPropertyBindingResult(this.realObject, this.root);
		PropertyDescriptor rootProp = this.beanDescriptor.getConstraintsForProperty(fieldName);
		if(rootProp==null)
			return;
		List<PropertyDescriptor> finalProp = new LinkedList<PropertyDescriptor>();
		if (rootProp.isCascaded())// if it's nested, scan all properties for
									// annotation
			finalProp.addAll(this.validator.getConstraintsForClass(rootProp.getElementClass())
					.getConstrainedProperties());
		else
			finalProp.add(rootProp);
		for (PropertyDescriptor prop : finalProp)
			for (final ConstraintDescriptor<?> desc : prop.getConstraintDescriptors()) {
				Annotation constraint = desc.getAnnotation();
				String className = this.beanDescriptor.getElementClass().getSimpleName();
				className = Character.toLowerCase(className.charAt(0)) + className.substring(1);
				String field = className + "." + prop.getPropertyName();
				String errorCode = constraint.annotationType().getSimpleName();
				Object[] errorArgs = this.validator.getArgumentsForConstraint(this.root, field, desc);
				String message = (String) desc.getAttributes().get(ANNOTATION_MESSAGE);
				if (INTERNAL.matcher(message).find())
					message = this.validatorFactory.getMessageInterpolator().interpolate(message, new Context() {

						public Object getValidatedValue() {
							return null;
						}

						public ConstraintDescriptor<?> getConstraintDescriptor() {
							return desc;
						}
					});
				String[] errorCodes = binding.resolveMessageCodes(errorCode, field);
				binding.addError(new FieldError(binding.getObjectName(), prop.getPropertyName(), null, false,
						errorCodes, errorArgs, message));
			}

		List<ObjectError> errors = binding.getAllErrors();
		StringBuilder customValidation = new StringBuilder();
		for (ObjectError e : errors) {
			String s = this.requestContext.getMessage(e, true);
			customValidation.append(s);
			if (!s.endsWith("."))
				customValidation.append(". ");

		}
		String onInvalid = String.format("this.setCustomValidity('%s')", customValidation.toString());
		fieldElement.setAttribute(ONINVALID_ATTR, onInvalid);
		fieldElement.setAttribute(ONCHANGE_ATTR, "this.setCustomValidity('')");
	}
}
