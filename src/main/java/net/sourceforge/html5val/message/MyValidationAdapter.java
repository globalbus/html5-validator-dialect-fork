package net.sourceforge.html5val.message;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.validation.metadata.ConstraintDescriptor;

import org.springframework.validation.Errors;
import org.springframework.validation.beanvalidation.SpringValidatorAdapter;

public class MyValidationAdapter extends SpringValidatorAdapter{
	public MyValidationAdapter(Validator targetValidator) {
		super(targetValidator);
	}
	@Override
	public Object[] getArgumentsForConstraint(String objectName,
			String field, ConstraintDescriptor<?> descriptor) {
		return super.getArgumentsForConstraint(objectName, field, descriptor);
	}
	@Override
	public void processConstraintViolations(
			Set<ConstraintViolation<Object>> violations, Errors errors) {
		super.processConstraintViolations(violations, errors);
	}
}