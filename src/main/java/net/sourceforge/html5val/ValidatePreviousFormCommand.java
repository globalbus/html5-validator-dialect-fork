package net.sourceforge.html5val;

import net.sourceforge.html5val.message.CustomValidity;

import org.thymeleaf.Arguments;
import org.thymeleaf.dom.Element;

import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import static net.sourceforge.html5val.FormElementFinder.findFormElements;
import static net.sourceforge.html5val.DomUtils.findPreviousElement;
import static net.sourceforge.html5val.DomUtils.removeElement;

public class ValidatePreviousFormCommand {

    private Arguments arguments;

    private Element element;

    private String attributeName;

    private Class jsr303AnnotatedClass;

    private Element formElement;

	private Object realObject;

	private CustomValidity customValidity;
    private static final Pattern ARRAY = Pattern.compile("(.*?)\\[\\d\\]");

    public ValidatePreviousFormCommand(Arguments arguments, Element element, String attributeName) {
        this.arguments = arguments;
        this.element = element;
        this.attributeName = attributeName;
    }

    public void execute() {
        readJsr303AnnotatedClass();
        prepareCustomValidity();
        findPreviousFormElement();
        processFields();
        removeElement(element);
    }

	private void readJsr303AnnotatedClass() {
        String attributeValue = element.getAttributeValue(attributeName);
        this.realObject =ExpressionUtil.evaluate(arguments, attributeValue);
        jsr303AnnotatedClass = realObject.getClass();
    }
    private void prepareCustomValidity() {
		if(ValidationPerformerFactory.isCustomValidityEnabled())
		{
			this.customValidity=new CustomValidity(arguments, element, attributeName, realObject);
		}
	}

    private void findPreviousFormElement() {
        formElement = findPreviousElement(element, "form");
    }

    private void processFields() {
        Set<Element> fields = findFormElements(formElement);
        for (Element field : fields) {
            processFieldValidation(field);
        }
    }

    private void processFieldValidation(Element fieldElement) {
		String type = this.element.getAttributeValue("type");
		if (type != null && type.equals("hidden"))
			return;// do not process hidden elements
		String fieldName = getFieldName(fieldElement);
		fieldName =ARRAY.matcher(fieldName).replaceFirst("$1");//strip array brackets
		
        List<Annotation> constraints = AnnotationExtractor.forClass(jsr303AnnotatedClass).getAnnotationsForField(fieldName);
        for (Annotation constraint : constraints) {
            ValidationPerformer processor = ValidationPerformerFactory.getPerformerFor(constraint);
            processor.putValidationCodeInto(constraint, fieldElement);
        }
        if(customValidity!=null)
        	customValidity.processField(fieldElement, fieldName);
    }

    private String getFieldName(Element fieldElement) {
        String TH_FIELD = "th:field"; // FIXME: get dynamic prefix
        if (fieldElement.getAttributeValue("name") != null) {
            return fieldElement.getAttributeValue("name");
        } else if (fieldElement.getAttributeValue(TH_FIELD) != null && fieldElement.getAttributeValue(TH_FIELD).startsWith("*")) {
            String value = fieldElement.getAttributeValue(TH_FIELD);
            return value.substring(2, value.length() - 1);
        }
        return null;
    }
}
