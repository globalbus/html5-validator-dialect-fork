package net.sourceforge.html5val;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.validation.Valid;

import static net.sourceforge.html5val.EmptyChecker.empty;

public class AnnotationExtractor {

	private Class targetClass;
	private String targetFieldName;
	private FieldFinder fieldFinder = new FieldFinder();

	private AnnotationExtractor(Class annotatedClass) {
		this.targetClass = annotatedClass;
	}

	public static AnnotationExtractor forClass(Class annotatedClass) {
		return new AnnotationExtractor(annotatedClass);
	}

	/**
	 * Return the annotations forClass a class field.
	 */
	public List<Annotation> getAnnotationsForField(String fieldName) {
		this.targetFieldName = fieldName;
		if (empty(targetFieldName)) {
			return new ArrayList<Annotation>();
		}
		return fieldAnnotations();
	}

	/**
	 * Return annotations for a given field name
	 */
	private List<Annotation> fieldAnnotations() {
		Field field = fieldFinder.findField(targetClass, targetFieldName);
		if (field != null) {
			List<Annotation> annotations = Arrays.asList(field.getAnnotations());
			List<Annotation> toAdd = new ArrayList<Annotation>();
			for (Annotation a : annotations)
				if (a.annotationType().isAssignableFrom(Valid.class)) {
					toAdd.addAll(new AnnotationExtractor(getType(field)).getAnnotationsForAllFields());
				} else
					toAdd.add(a);
			return toAdd;
		} else {
			return Collections.EMPTY_LIST;
		}
	}

	public Class<?> getType(Field field) {
		Class<?> type = field.getType();
		if (type.isArray())
			return type.getComponentType();
		else if (Collection.class.isAssignableFrom(type)) {
			ParameterizedType generic = (ParameterizedType) field.getGenericType();
			return (Class<?>)generic.getActualTypeArguments()[0];
		}
		return type;
	}

	public List<Annotation> getAnnotationsForAllFields() {
		List<Annotation> toAdd = new ArrayList<Annotation>();
		for (Field field : fieldFinder.findAllFields(targetClass))
			toAdd.addAll(Arrays.asList(field.getAnnotations()));
		return toAdd;
	}
}
