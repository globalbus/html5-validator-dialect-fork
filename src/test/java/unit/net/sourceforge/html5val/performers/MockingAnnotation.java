package unit.net.sourceforge.html5val.performers;

import java.lang.annotation.Annotation;

import org.jmock.Expectations;
import org.jmock.Mockery;

public class MockingAnnotation {
	public static <T extends Annotation> T buildContext(final Class<T> clazz, Mockery context){
		final T notEmptyAnnotation = context.mock(clazz);
    	context.checking(new Expectations(){{allowing(notEmptyAnnotation).annotationType();
    	will(returnValue(clazz));
    	}});
    	return notEmptyAnnotation;
	}
}
